# Positive Usernames

Easily create usernames with positive adjectives, like 'happy-charles'.

### How to use

Install

```sh
yarn add positive-usernames
```

Import into your code

```javascript
const getUsername = require('positive-usernames')
```

And use like so:

```javascript
const username = getUsername('sam')
// energetic-sam
```

This package will also check to see if you have passed in an email address, and if so if will trim it. So 'jennifer@doe.com' will become 'jennifer'.
No rocket science here, i'm afraid.

### Contributing

Any updates are welcome - create a MR in the [repository](https://gitlab.com/travis-projects/positive-usernames.git)
