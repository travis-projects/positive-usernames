const adjectives = require('./adjectives.json')

function getAdjective() {
  const randomNumber = Math.floor(Math.random()*adjectives.length)
  return adjectives[randomNumber]
}

function checkForEmail (input) {
  return input.includes('@') ? input.split('@')[0] : input
}

function createUsername (input) {
  const name = checkForEmail(input)
  return `${getAdjective()}-${name}`
}

module.exports = createUsername
